# edit 
 
##Summary
This is a script with which you can make simple encrypted notes

##Usage
edit <filename>

If the file exists already it will be decrypted, if not it 
will be created and you will be asked for the password twice
 
##Caveat
This script creates temporary files which will be readable.
Set your TEMPDIR environment variable to something like

export TEMPDIR=${HOME}/temp

The use of temporary files remains a weak point however.

This script should NOT be seen as a serious replacement for
the likes of GPG

