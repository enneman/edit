#!/bin/bash
#===============================================================================
#
#          FILE:  edit
# 
#         USAGE:  edit [-r] <filename>
# 
#   DESCRIPTION:  Encrypt and decrypt files with openssl
# 
#       OPTIONS:  -r (rekey password) 
#  REQUIREMENTS:  openssl shred 
#          BUGS:  temp files are readable during edit!
#         NOTES:  if you use vim as your editor
#                 then dont forget to set the following
#                 in your .vimrc:
#                 set nobackup
#                 set nowritebackup
#                 set noswapfile
#                 This to avoid unencrypted copies beside the TMPFILE to hit
#                 your disk.
#                 
#                 Also set your TMPDIR environment variabele to something
#                 like: export TMPDIR=${HOME}/tmp and create this
#                 directory beforehand. mktemp uses this directory
#                 to create your temporary decrypted files
#                 By default mktemp will use /tmp !!!!
#                 
#                 Note: if you create a new file you'll have to enter the
#                 password twice.
# 
#
#        AUTHOR:  Enneman 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  01/20/2013 08:17:51 PM CET
#      REVISION:  $Id:$ 
#===============================================================================


die() 
{ 
    echo "$*"
    exit 1 
}

CIPHER='-aes-256-cbc -a -salt'
TMPFILE=$(mktemp -t $(basename $0).XXXXXX) || die "mktemp failed"
RC=1

# trap signals 
trap "shred -u -z $TMPFILE; stty echo; exit 1" SIGHUP SIGINT SIGTERM



# check if -r option is set
while getopts "r" Option
do
	case $Option in
		'r' )
			shift $(($OPTIND -1)) 
			[[ ! -f $1 ]] && die "File $1 does not exist"
			RC=1
			while [[ $RC -ne 0 ]]; do
				read -s -p "Old Passphrase: " PASSPHRASE; echo
    				openssl enc -d $CIPHER -in "$1" -out "$TMPFILE" -pass stdin <<< "$PASSPHRASE"
    				RC=$?
			done

			RC=1
			while [[ $RC -ne 0 ]]; do
				read -s -p "New Passphrase: " PASSPHRASE; echo
                                openssl enc $CIPHER -in "$TMPFILE" -out "$1" -pass stdin <<< "$PASSPHRASE"
    				RC=$?
			done
			exit
			;;
	esac	
done

# check if file exists, if not ask if it should be created
if [[ ! -f $1 ]]; then
    echo "File $1 does not exist.. create it? (y/n) "
    read -r ANSWER
    [[ $ANSWER !=  "y" ]] && die "No encrypted file made." 
    read -s -p "Passphrase: " PASSPHRASE; echo
    # create encrypted file
    openssl enc $CIPHER -out "$1" -pass stdin <<< "$PASSPHRASE"
fi

# check number of arguments
[[ $# -ne 1 ]] && die "Usage: $0 <filenaam>"

# loop until correct password is entered
while [[ $RC -ne 0 ]]; do
    read -s -p "Passphrase: " PASSPHRASE; echo
    # decrypt the encrypted file to the temporary file
    openssl enc -d $CIPHER -in "$1" -out "$TMPFILE" -pass stdin <<< "$PASSPHRASE"
    RC=$?
done

# edit the temporary file with the editor set in the environment variabele
# EDITOR 
$EDITOR "$TMPFILE"

# encrypt the temporary file and write it back to the original file
[[ $? -eq 0 ]] && openssl enc $CIPHER -in "$TMPFILE" -out "$1" -pass stdin <<< "$PASSPHRASE"

# destroy and remove the temporary file if encoding successfull
if [[ $? -eq 0 ]]; then
	shred -u -z "$TMPFILE" 
else
	die "Error while encoding, temporary file $TMPFILE NOT removed"
fi
clear
