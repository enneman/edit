PROGRAM=edit
PREFIX=${HOME}

gitpush:
	git push -u origin master

install: $(PROGRAM)
	@echo "Installing edit..."
	install -m 0755 $(PROGRAM) $(PREFIX)/bin

clean: 
	rm *~
